dynaugeas
=========

Extended (in-memory) Augeas trees for dynamic (machine) state representation.
